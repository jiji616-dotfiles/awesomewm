pcall(require, "luarocks.loader")

local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
require("awful.hotkeys_popup.keys")
require("awful.autofocus")

local batterywidget = require("battery")
local networkwidget = require("network")
local weatherwidget = require("weather")
local taglist = require("taglist")
-- local layoutbox     = require("layoutbox")

if awesome.startup_errors then
	naughty.notify({
		preset = naughty.config.presets.critical,
		title = "Oops, there were errors during startup!",
		text = awesome.startup_errors,
	})
end

quit = function()
	-- for pid in ipairs(pids) do
	--     awesome.spawn.easy_async('kill ' ..pid)
	-- end
	awesome.quit()
end

-- Handle runtime errors after startup
do
	local in_error = false
	awesome.connect_signal("debug::error", function(err)
		-- Make sure we don't go into an endless error loop
		if in_error then
			return
		end
		in_error = true
		naughty.notify({
			preset = naughty.config.presets.critical,
			title = "Oops, an error happened!",
			text = tostring(err),
		})
		in_error = false
	end)
end

-- Variables
beautiful.init("/home/julia/.config/awesome/black/theme.lua")
terminal = "urxvt"
editor = os.getenv("EDITOR") or "nvi"
editor_cmd = terminal .. " -e " .. editor
modkey = "Mod4"

-- Layouts
awful.layout.layouts = {
	awful.layout.suit.tile,
	awful.layout.suit.tile.left,
	awful.layout.suit.tile.bottom,
	awful.layout.suit.tile.top,
	awful.layout.suit.max,
	awful.layout.suit.floating,
}

-- Menu
myawesomemenu = {
	{
		"hotkeys",
		function()
			hotkeys_popup.show_help(nil, awful.screen.focused())
		end,
	},
	{ "manual", terminal .. " -e man awesome" },
	{ "edit config", editor_cmd .. " " .. awesome.conffile },
	{ "restart", awesome.restart },
	{
		"quit",
		function()
			awesome.quit()
		end,
	},
}

musicmenu = {
	{ "pause", "mpc toggle" },
}

mymainmenu = awful.menu({
	items = {
		{ "term", terminal },
		{ "ncmpcpp", terminal .. " -e ncmpcpp" },
		{ "vim", terminal .. " -e nvim" },
		{ "htop", terminal .. " -e htop" },
		{ "web", "firefox" },
		{ "awesome", myawesomemenu },
	},
})

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
	awful.button({}, 1, function(t)
		t:view_only()
	end),
	awful.button({ modkey }, 1, function(t)
		if client.focus then
			client.focus:move_to_tag(t)
		end
	end),
	awful.button({}, 3, awful.tag.viewtoggle),
	awful.button({ modkey }, 3, function(t)
		if client.focus then
			client.focus:toggle_tag(t)
		end
	end),
	awful.button({}, 4, function(t)
		awful.tag.viewnext(t.screen)
	end),
	awful.button({}, 5, function(t)
		awful.tag.viewprev(t.screen)
	end)
)

local tasklist_buttons = gears.table.join(
	awful.button({}, 3, function()
		awful.menu.client_list({ theme = { width = 250 } })
	end),
	awful.button({}, 4, function()
		awful.client.focus.byidx(1)
	end),
	awful.button({}, 5, function()
		awful.client.focus.byidx(-1)
	end)
)

local function set_wallpaper(s)
	-- Wallpaper
	if beautiful.wallpaper then
		local wallpaper = beautiful.wallpaper
		-- If wallpaper is a function, call it with the screen
		if type(wallpaper) == "function" then
			wallpaper = wallpaper(s)
		end
		gears.wallpaper.tiled(wallpaper, s)
	end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
	-- Wallpaper
	-- set_wallpaper(s)

	awful.tag.add("1", {
		name = "web",
		layout = awful.layout.layouts[5],
		master_width_factor = 0.6,
		gap_single_client = true,
		screen = s,
		selected = true,
	})
	awful.tag.add("2", {
		name = "office",
		layout = awful.layout.layouts[5],
		master_width_factor = 0.6,
		gap_single_client = true,
		screen = s,
	})
	awful.tag.add(
		"3",
		{ layout = awful.layout.layouts[1], master_width_factor = 0.6, gap_single_client = true, screen = s }
	)
	awful.tag.add(
		"4",
		{ layout = awful.layout.layouts[1], master_width_factor = 0.6, gap_single_client = true, screen = s }
	)
	awful.tag.add(
		"5",
		{ layout = awful.layout.layouts[1], master_width_factor = 0.6, gap_single_client = true, screen = s }
	)
	awful.tag.add(
		"6",
		{ layout = awful.layout.layouts[1], master_width_factor = 0.6, gap_single_client = true, screen = s }
	)
	awful.tag.add("7", {
		name = "pass",
		layout = awful.layout.layouts[1],
		master_width_factor = 0.6,
		gap_single_client = true,
		screen = s,
	})
	awful.tag.add("8", {
		name = "music",
		layout = awful.layout.layouts[1],
		master_width_factor = 0.6,
		gap_single_client = true,
		screen = s,
	})
	awful.tag.add("9", {
		name = "comm",
		layout = awful.layout.layouts[1],
		master_width_factor = 0.6,
		gap_single_client = true,
		screen = s,
	})

	s.mytaglist = taglist.new({
		screen = s,
		underline = 5,
	})

	-- Create a tasklist widget
	s.mytasklist = awful.widget.tasklist({
		screen = s,
		filter = awful.widget.tasklist.filter.focused,
		buttons = tasklist_buttons,
	})

	-- Create the wibox
	s.mywibox = awful.wibar({
		height = beautiful.wibar_height,
		position = "top",
		screen = s,
	})

	-- mybattery = batterywidget.new({
	--   bg = "#1f1f1f"
	-- })

	-- mynetwork = networkwidget.new({
	--   bg = "#1f1f1f"
	-- })

	-- myweather = weatherwidget.new({
	--   bg = "#1f1f1f"
	-- })

	-- Add widgets to the wibox
	s.mywibox:setup({
		{
			spacing = 0,
			spacing_widget = {
				shape = gears.shape.rectangle,
			},
			layout = wibox.layout.align.horizontal,

			-- left
			{
				widget = wibox.container.margin,
				left = 0,
				right = 0,
				{
					widget = wibox.container.background,
					shape = gears.shape.rectangle,
					shape_border_width = 0,
					shape_border_color = "#afafaf",
					shape_clip = true,
					bg = beautiful.wibar_bg,
					radius = 2,
					s.mytaglist,
				},
			},

			-- middle
			{
				widget = wibox.container.margin,
				left = 0,
				right = 0,
				{
					widget = wibox.container.background,
					shape = gears.shape.rectangle,
					shape_border_width = 0,
					shape_border_color = "#afafaf",
					shape_clip = true,
					bg = beautiful.wibar_bg,
					radius = 2,
					s.mytasklist,
				},
			},

			--right
			{
				layout = wibox.layout.fixed.horizontal,
				spacing = 20,
				{
					widget = wibox.container.background,
					shape = gears.shape.rectangle,
					base_width = 500,
					shape_border_width = 0,
					shape_border_color = "#afafaf",
					shape_clip = true,
					bg = beautiful.widget_bg,
					radius = 2,
					{
						widget = wibox.container.margin,
						top = 10,
						bottom = 10,
						left = 10,
						right = 10,
						bg = beautiful.widget_bg,
						wibox.widget.systray(),
					},
				},
				{
					widget = wibox.container.background,
					shape = gears.shape.rectangle,
					shape_border_width = 0,
					shape_border_color = "#afafaf",
					shape_clip = true,
					bg = beautiful.widget_bg,
					radius = 2,
					mytextclock,
				},
			},
		},
		widget = wibox.container.margin,
		left = 0,
		right = 0,
		top = 0,
		bottom = 0,
	})
end)
--

-- Mouse bindings
root.buttons(gears.table.join(
	awful.button({}, 3, function()
		mymainmenu:toggle()
	end),
	awful.button({}, 4, awful.tag.viewnext),
	awful.button({}, 5, awful.tag.viewprev)
))
--

-- Key bindings
globalkeys = gears.table.join(
	awful.key({ modkey }, "s", hotkeys_popup.show_help, { description = "show help", group = "awesome" }),
	awful.key({ modkey }, "Tab", awful.tag.history.restore, { description = "go back", group = "tag" }),
	awful.key({ modkey }, "j", function()
		awful.client.focus.byidx(1)
	end, { description = "focus next by index", group = "client" }),
	awful.key({ modkey }, "k", function()
		awful.client.focus.byidx(-1)
	end, { description = "focus previous by index", group = "client" }),

	-- Layout manipulation
	awful.key({ modkey, "Shift" }, "j", function()
		awful.client.swap.byidx(1)
	end, { description = "swap with next client by index", group = "client" }),
	awful.key({ modkey, "Shift" }, "k", function()
		awful.client.swap.byidx(-1)
	end, { description = "swap with previous client by index", group = "client" }),
	awful.key({ modkey, "Control" }, "j", function()
		awful.screen.focus_relative(1)
	end, { description = "focus the next screen", group = "screen" }),
	awful.key({ modkey, "Control" }, "k", function()
		awful.screen.focus_relative(-1)
	end, { description = "focus the previous screen", group = "screen" }),
	awful.key({ modkey }, "u", awful.client.urgent.jumpto, { description = "jump to urgent client", group = "client" }),

	-- Standard program
	awful.key({ modkey, "Shift" }, "Return", function()
		awful.spawn(terminal)
	end, { description = "open a terminal", group = "launcher" }),
	awful.key({ modkey, "Shift" }, "q", function()
		quit()
	end, { description = "quit awesome", group = "awesome" }),
	awful.key({ modkey, "Shift" }, "h", function()
		awful.client.incwfact(0.1, nil, true)
	end, { description = "increase size of tiled window", group = "layout" }),
	awful.key({ modkey, "Shift" }, "l", function()
		awful.client.incwfact(-0.1, nil, true)
	end, { description = "decrease size of tiled window", group = "layout" }),
	awful.key({ modkey, "Shift" }, "o", function()
		awful.client.setwfact(0, nil, true)
	end, { description = "decrease size of tiled window", group = "layout" }),
	awful.key({ modkey }, "l", function()
		awful.tag.incmwfact(0.05)
	end, { description = "increase master width factor", group = "layout" }),
	awful.key({ modkey }, "h", function()
		awful.tag.incmwfact(-0.05)
	end, { description = "decrease master width factor", group = "layout" }),
	awful.key({ modkey, "Shift" }, "i", function()
		awful.tag.incnmaster(0, nil, true)
	end, { description = "increase the number of master clients", group = "layout" }),
	awful.key({ modkey, "Shift" }, "d", function()
		awful.tag.incnmaster(-1, nil, true)
	end, { description = "decrease the number of master clients", group = "layout" }),
	awful.key({ modkey, "Control" }, "h", function()
		awful.tag.incncol(1, nil, true)
	end, { description = "increase the number of columns", group = "layout" }),
	awful.key({ modkey, "Control" }, "l", function()
		awful.tag.incncol(-1, nil, true)
	end, { description = "decrease the number of columns", group = "layout" }),
	awful.key({ modkey }, "space", function()
		awful.layout.inc(1)
	end, { description = "select next", group = "layout" }),
	awful.key({ modkey, "Shift" }, "space", function()
		awful.layout.inc(-1)
	end, { description = "select previous", group = "layout" })
)

clientkeys = gears.table.join(
	awful.key({ modkey }, "q", function(c)
		c:kill()
	end, { description = "close", group = "client" }),
	awful.key(
		{ modkey, "Control" },
		"space",
		awful.client.floating.toggle,
		{ description = "toggle floating", group = "client" }
	),
	awful.key({ modkey }, "Return", function(c)
		if client.focus == awful.client.getmaster() then
			awful.client.setslave(c)
		else
			awful.client.setmaster(c)
		end
	end, { description = "move to master", group = "client" }),
	awful.key({ modkey }, "o", function(c)
		c:move_to_screen()
	end, { description = "move to screen", group = "client" }),
	awful.key({ modkey }, "t", function(c)
		c.ontop = not c.ontop
	end, { description = "toggle keep on top", group = "client" }),
	-- awful.key({ modkey,           }, "n", function (c) c.minimized = true end, {description = "minimize", group = "client"}),
	awful.key({ modkey }, "m", function(c)
		c.maximized = not c.maximized
		c:raise()
	end, { description = "(un)maximize", group = "client" }),
	awful.key({ modkey, "Control" }, "m", function(c)
		c.maximized_vertical = not c.maximized_vertical
		c:raise()
	end, { description = "(un)maximize vertically", group = "client" }),
	awful.key({ modkey, "Shift" }, "m", function(c)
		c.maximized_horizontal = not c.maximized_horizontal
		c:raise()
	end, { description = "(un)maximize horizontally", group = "client" })
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.

for i = 1, 9 do
	globalkeys = gears.table.join(
		globalkeys,
		-- View tag only.
		awful.key({ modkey }, "#" .. i + 9, function()
			local screen = awful.screen.focused()
			local tag = screen.tags[i]
			if tag then
				tag:view_only()
			end
		end, { description = "view tag #" .. i, group = "tag" }),
		-- Toggle tag display.
		awful.key({ modkey, "Control" }, "#" .. i + 9, function()
			local screen = awful.screen.focused()
			local tag = screen.tags[i]
			if tag then
				awful.tag.viewtoggle(tag)
			end
		end, { description = "toggle tag #" .. i, group = "tag" }),
		-- Move client to tag.
		awful.key({ modkey, "Shift" }, "#" .. i + 9, function()
			if client.focus then
				local tag = client.focus.screen.tags[i]
				if tag then
					client.focus:move_to_tag(tag)
				end
			end
		end, { description = "move focused client to tag #" .. i, group = "tag" }),
		-- Toggle tag on focused client.
		awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9, function()
			if client.focus then
				local tag = client.focus.screen.tags[i]
				if tag then
					client.focus:toggle_tag(tag)
				end
			end
		end, { description = "toggle focused client on tag #" .. i, group = "tag" })
	)
end

clientbuttons = gears.table.join(
	awful.button({}, 1, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
	end),
	awful.button({ modkey }, 1, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
		awful.mouse.client.move(c)
	end),
	awful.button({ modkey }, 3, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
		awful.mouse.client.resize(c)
	end)
)

-- Set keys
root.keys(globalkeys)

-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
	-- All clients will match this rule.
	{
		rule = {},
		properties = {
			border_width = beautiful.border_width,
			border_color = beautiful.border_normal,
			focus = awful.client.focus.filter,
			raise = true,
			keys = clientkeys,
			buttons = clientbuttons,
			screen = awful.screen.preferred,
			placement = awful.placement.no_overlap + awful.placement.no_offscreen,
			size_hints_honor = false,
		},
	},
	{
		rule_any = { class = { "Oblogout" } },
		properties = { fullscreen = true },
	},

	-- Floating clients.
	{
		rule_any = {
			instance = {
				"DTA", -- Firefox addon DownThemAll.
				"copyq", -- Includes session name in class.
				"pinentry",
			},
			class = {
				"Arandr",
				"Blueman-manager",
				"Gpick",
				"Kruler",
				"MessageWin", -- kalarm.
				"Sxiv",
				"Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
				"Wpa_gui",
				"veromix",
				"xtightvncviewer",
			},
			-- Note that the name property shown in xprop might be set slightly after creation of the client
			-- and the name shown there might not match defined rules here.
			name = {
				"Event Tester", -- xev.
			},
			role = {
				"AlarmWindow", -- Thunderbird's calendar.
				"ConfigManager", -- Thunderbird's about:config.
				"pop-up", -- e.g. Google Chrome's (detached) Developer Tools.
			},
		},
		properties = { floating = true },
	},

	-- Add titlebars to normal clients and dialogs
	{
		rule_any = { type = { "normal", "dialog" } },
		properties = { titlebars_enabled = beautiful.titlebar_enabled },
	},

	{ rule = { class = "Firefox" }, properties = { tag = "web" } },
	{ rule = { class = "Slack" }, properties = { tag = "comm" } },
	{ rule = { class = "[Ss]potify" }, properties = { tag = "music" } },
	{ rule = { class = "keepassxc" }, properties = { tag = "pass" } },
	{ rule = { class = "Chromium" }, properties = { tag = "office" } },
	{ rule = { class = "Thunderbird" }, properties = { tag = "office" } },
	{ rule = { class = "Nitrogen" }, properties = { floating = true } },
	{ rule = { class = "Lxappearance" }, properties = { floating = true } },

	{
		rule_any = {
			floating = {
				true,
			},
		},
		properties = {
			placement = awful.placement.centered,
		},
	},
}

-- Keep floating windows on top
client.connect_signal("property::floating", function(c)
	if c.floating then
		c.ontop = true
	elseif not c.floating then
		c.ontop = false
	end
end)

-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
	-- Set the windows at the slave,
	-- i.e. put it at the end of others instead of setting it master.
	if not awesome.startup then
		awful.client.setslave(c)
	end

	if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
		-- Prevent clients from being unreachable after screen count changes.
		awful.placement.no_offscreen(c)
	end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
	-- buttons for the titlebar
	local buttons = gears.table.join(
		awful.button({}, 1, function()
			c:emit_signal("request::activate", "titlebar", { raise = true })
			awful.mouse.client.move(c)
		end),
		awful.button({}, 3, function()
			c:emit_signal("request::activate", "titlebar", { raise = true })
			awful.mouse.client.resize(c)
		end)
	)

	awful
		.titlebar(c, {
			position = "left",
		})
		:setup({
			{
				{
					{ -- Left
						buttons = buttons,
						layout = wibox.layout.fixed.horizontal,
						{
							widget = wibox.widget.textbox,
							text = " ",
						},
						{ -- Title
							align = "center",
							widget = awful.titlebar.widget.titlewidget(c),
						},
					},
					{ -- Middle
						buttons = buttons,
						layout = wibox.layout.flex.horizontal,
					},
					{ -- Right
						awful.titlebar.widget.floatingbutton(c),
						awful.titlebar.widget.maximizedbutton(c),
						awful.titlebar.widget.stickybutton(c),
						awful.titlebar.widget.ontopbutton(c),
						awful.titlebar.widget.closebutton(c),
						layout = wibox.layout.fixed.horizontal(),
					},
					layout = wibox.layout.align.horizontal,
				},
				widget = wibox.container.rotate,
				direction = "east",
			},
			widget = wibox.container.margin,
			right = 1,
			color = beautiful.border_normal,
		})
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
	c:emit_signal("request::activate", "mouse_enter", { raise = false })
end)

client.connect_signal("focus", function(c)
	c.border_color = beautiful.border_focus
end)
client.connect_signal("unfocus", function(c)
	c.border_color = beautiful.border_normal
end)

-- Disable borders for maximized windows
client.connect_signal("property::maximized", function(c)
	if c.maximized then
		c.border_width = 0
	else
		c.border_width = beautiful.border_width
	end
end)

awesome.register_xproperty("_COMPTON_SHADOW", "string")
client.connect_signal("property::floating", function(c)
	if not c.floating then
		c:set_xproperty("_COMPTON_SHADOW", "0")
	elseif c.floating then
		c:set_xproperty("_COMPTON_SHADOW", "1")
	end
end)

-- client.connect_signal("property::floating", function(c)
--   if c.floating then
--     c.shape = function(cr,w,h) gears.shape.rounded_rect(cr,w,h,10) end
--     c.border_width = 0
--   else
--     c.shape = function(cr,w,h) gears.shape.rounded_rect(cr,w,h,4) end
--     c.border_width = beautiful.border_width
--   end
-- end)

client.connect_signal("manage", function(c)
	c.shape = function(cr, w, h)
		gears.shape.rounded_rect(cr, w, h, beautiful.rounded_corner)
	end
end)

-- awful.keygrabber {
--   keybindings = {
--     {{ modkey, "Control" }, '1', function() screen = awful.screen.focused() awful.tag.viewtoggle(screen.tags[1]) end},
--     {{ modkey, "Control" }, '2', function() screen = awful.screen.focused() awful.tag.viewtoggle(screen.tags[2]) end},
--     {{ modkey, "Control" }, '3', function() screen = awful.screen.focused() awful.tag.viewtoggle(screen.tags[3]) end},
--     {{ modkey, "Control" }, '4', function() screen = awful.screen.focused() awful.tag.viewtoggle(screen.tags[4]) end},
--     {{ modkey, "Control" }, '5', function() screen = awful.screen.focused() awful.tag.viewtoggle(screen.tags[5]) end},
--     {{ modkey, "Control" }, '6', function() screen = awful.screen.focused() awful.tag.viewtoggle(screen.tags[6]) end},
--     {{ modkey, "Control" }, '7', function() screen = awful.screen.focused() awful.tag.viewtoggle(screen.tags[7]) end},
--     {{ modkey, "Control" }, '8', function() screen = awful.screen.focused() awful.tag.viewtoggle(screen.tags[8]) end},
--     {{ modkey, "Control" }, '9', function() screen = awful.screen.focused() awful.tag.viewtoggle(screen.tags[9]) end},
--   },
--   stop_key           = modkey,
--   stop_event         = release,
--   export_keybindings = true,
-- }
