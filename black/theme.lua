local themes_path = "/home/julia/.config/awesome/"
local dpi = require("beautiful.xresources").apply_dpi

local theme = {}

theme.wallpaper = themes_path .. "black/background.png"
theme.font      = "Envy Code R 8"

-- Colors
theme.bg_normal  = "#151515"
theme.bg_focus   = "#151515"
theme.bg_urgent  = "#151515"
theme.bg_mouseover = "#1a1a1a"
theme.fg_mouseover = "#e5e5e5"

theme.fg_normal  = "#e5e5e5"
theme.fg_focus   = "#f39d21"
theme.fg_urgent  = "#e5e5e5"

theme.border_normal = "#6f6f6f"
theme.border_focus  = "#d7d0c7"
theme.border_marked = "#a0cf5d"

theme.titlebar_fg_focus   = "#e5e5e5"
theme.titlebar_bg_normal  = "#2a2a2a"
theme.titlebar_bg_focus = "#3a3a3a"

theme.tag_bg_mouseover = "#202020"
theme.tag_bg_selected = "#202020"
theme.tag_bg_normal = theme.bg_normal

theme.tag_underline_normal = "#7B4F00"
theme.tag_underline_active = "#f39d21"
theme.tag_underline_mouseover = "#7B4F00"
theme.tag_underline_mouseover_active = "#e1aa5d"

theme.menu_bg_normal = "#000000"
theme.menu_bg_focus  = "#d23d3d"

-- Borders
theme.useless_gap   = dpi(3)
theme.border_width  = dpi(2)
theme.systray_icon_spacing = dpi(5)
theme.rounded_corner = 0
theme.wibar_height = dpi(25)
theme.wibar_bg = "#000000"

-- Menu
theme.menu_height = dpi(15)
theme.menu_width  = dpi(70)
theme.menu_border_width = dpi(5)
theme.menu_border_color = "#000000"
theme.menu_submenu = ">"

-- Widgets
theme.widget_bg = "#202020"
theme.bg_systray = theme.widget_bg

-- Misc
theme.awesome_icon           = themes_path .. "black/awesome-icon.png"

-- Layout
theme.layout_tile       = themes_path .. "black/layouts/tile.png"
theme.layout_tileleft   = themes_path .. "black/layouts/tileleft.png"
theme.layout_tilebottom = themes_path .. "black/layouts/tilebottom.png"
theme.layout_tiletop    = themes_path .. "black/layouts/tiletop.png"
theme.layout_fairv      = themes_path .. "black/layouts/fairv.png"
theme.layout_fairh      = themes_path .. "black/layouts/fairh.png"
theme.layout_spiral     = themes_path .. "black/layouts/spiral.png"
theme.layout_dwindle    = themes_path .. "black/layouts/dwindle.png"
theme.layout_max        = themes_path .. "black/layouts/max.png"
theme.layout_fullscreen = themes_path .. "black/layouts/fullscreen.png"
theme.layout_magnifier  = themes_path .. "black/layouts/magnifier.png"
theme.layout_floating   = themes_path .. "black/layouts/floating.png"
theme.layout_cornernw   = themes_path .. "black/layouts/cornernw.png"
theme.layout_cornerne   = themes_path .. "black/layouts/cornerne.png"
theme.layout_cornersw   = themes_path .. "black/layouts/cornersw.png"
theme.layout_cornerse   = themes_path .. "black/layouts/cornerse.png"
theme.tasklist_disable_icon = true

-- Titlebar
theme.titlebar_enabled = false
theme.titlebar_close_button_focus  = themes_path .. "black/titlebar/close_focus.png"
theme.titlebar_close_button_normal = themes_path .. "black/titlebar/close_normal.png"

theme.titlebar_minimize_button_normal = themes_path .. "default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = themes_path .. "default/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_focus_active  = themes_path .. "black/titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active = themes_path .. "black/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive  = themes_path .. "black/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive = themes_path .. "black/titlebar/ontop_normal_inactive.png"

theme.titlebar_sticky_button_focus_active  = themes_path .. "black/titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active = themes_path .. "black/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive  = themes_path .. "black/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive = themes_path .. "black/titlebar/sticky_normal_inactive.png"

theme.titlebar_floating_button_focus_active  = themes_path .. "black/titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active = themes_path .. "black/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive  = themes_path .. "black/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive = themes_path .. "black/titlebar/floating_normal_inactive.png"

theme.titlebar_maximized_button_focus_active  = themes_path .. "black/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active = themes_path .. "black/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path .. "black/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = themes_path .. "black/titlebar/maximized_normal_inactive.png"

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
