#!/bin/sh

convert zenburn-background.png -fill '#202020' -opaque '#1E231F' 1.png
convert 1.png -fill '#FECBD7' -opaque '#F0E0AD' 2.png
convert 2.png -fill '#A2CBBB' -opaque '#ACBD93' background.png

if [[ -f '1.png' ]] ; then
	rm '1.png'
fi

if [[ -f '2.png' ]] ; then
	rm '2.png'
fi
