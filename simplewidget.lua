local wibox     = require("wibox")
local gears     = require("gears")
local awful     = require("awful")
local beautiful = require("beautiful")

local simplewidget = { }

function simplewidget.new(args)
	local simplebutton = wibox.layout.fixed.horizontal()
	local simpletext   = wibox.widget.textbox('')

	simplebutton:add(simpletext)

	return simplebutton
end

return simplewidget
