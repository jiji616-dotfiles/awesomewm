local capi      = {
  screen  = screen,
  awesome = awesome,
  client  = client
}
local wibox     = require("wibox")
local gears     = require("gears")
local awful     = require("awful")
local beautiful = require("beautiful")

local taglist = { }

local screen = nil

local function get_screen(s)
  return s and capi.screen[s]
end

function taglist.new(args)

  margins    = args.margins or 0
  underline  = args.underline or 3

  margins_in = 10

  screen = screen or get_screen(args.screen)
  widget = wibox.layout.fixed.horizontal()

  for i, tag in ipairs(screen.tags) do
    local tag_button    = wibox.layout.flex.vertical()
    local tag_text      = wibox.widget.textbox(tostring(tag.name))
    local tag_margins   = wibox.container.margin(tag_text, margins_in, margins_in)
    local tag_color     = wibox.container.background(tag_margins)
    local tag_underline = wibox.container.margin(tag_color, 0, 0, 0, underline)
    tag_button:add(wibox.container.margin(tag_underline, margins, margins))

    tag_button:connect_signal("button::press", function(_, _, _, button)
      if button == 1 then
        tag:view_only()
      elseif button == 3 then
        awful.tag.viewtoggle(tag)
      end
    end)

    tag_button:connect_signal("mouse::enter", function()
      tag_color:set_bg(beautiful.tag_bg_mouseover)
      if tag.selected then
        tag_underline:set_color(beautiful.tag_underline_mouseover_active)
      else
        tag_underline:set_color(beautiful.tag_underline_mouseover)
      end
    end)

    tag_button:connect_signal("mouse::leave", function()
      tag_color:set_bg(beautiful.bg_normal)
      if tag.selected then
        tag_underline:set_color(beautiful.tag_underline_active)
        tag_color:set_bg(beautiful.tag_bg_selected)
      else
        tag_underline:set_color(beautiful.tag_underline_normal)
      end
    end)

    reload_tags = function()
      if tag.selected then
        tag_underline:set_color(beautiful.tag_underline_active)
        tag_color:set_bg(beautiful.tag_bg_selected)
      else
        tag_underline:set_color(beautiful.tag_underline_normal)
        tag_color:set_bg(beautiful.tag_bg_normal)
      end
      if tag.selected or #tag:clients() > 0 then
        tag_button:set_visible(true)
      else
        tag_button:set_visible(false)
      end
    end

    client.connect_signal("manage", reload_tags)
    screen:connect_signal("tag::history::update", reload_tags)
    tag:connect_signal("tagged", reload_tags)
    tag:connect_signal("untagged", reload_tags)

    widget:add(tag_button)
  end

  return widget
end

return taglist
