local wibox     = require("wibox")
local gears     = require("gears")
local awful     = require("awful")
local beautiful = require("beautiful")

local network = { }

function network.new(args)

	bg = args.bg or "#000000"
	fg = args.fg or "#ffffff"
	mg = args.margins or 10

	local cmd = [[bash -c '
/home/julia/Skrypty/monitor-network.sh
']]

	local widget     = wibox.layout.fixed.horizontal()
	local text       = wibox.widget.textbox()
	local margins    = wibox.container.margin(text, mg, mg)
	local background = wibox.container.background(margins)

	widget:add(background)
	background:set_bg(bg)
	text:set_valign('center')


	pid = awful.spawn.with_line_callback(cmd, {
		stdout = function(line)
			text:set_text(line)
		end
	})

	return widget
end

function network.get_pid()
	return pid
end

return network
