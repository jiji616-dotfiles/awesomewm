local capi      = {
  screen  = screen,
  awesome = awesome,
  client  = client
}
local wibox     = require("wibox")
local gears     = require("gears")
local awful     = require("awful")
local beautiful = require("beautiful")

local layoutbox = { }

local screen = nil

local function get_screen(s)
  return s and capi.screen[s]
end

function layoutbox.new(args)

  margins    = args.margins or 3
  underline  = args.ul or 3

  margins_in = 10

  screen = screen or get_screen(args.screen)
  widget = wibox.layout.fixed.horizontal()

  local layout_button    = wibox.layout.flex.vertical()
  local layout_text      = wibox.widget.textbox(tostring(tag.name))
  local layout_image     = wibox.widget.imagebox(beautiful.layout_tile)
  local layout_box       = wibox.layout.stack(layout_image, layout_text)
  local layout_margins   = wibox.container.margin(layout_box, margins_in, margins_in)
  local layout_color     = wibox.container.background(tag_margins)
  tag_button:add(wibox.container.margin(tag_color, margins, margins))

  tag_button:connect_signal("button::press", function(_, _, _, button)
    if button == 1 then
      awful.layout.inc( 1)
    elseif button == 3 then
      awful.layout.inc(-1)
    end
  end)


  refresh_layouts = function ()
    name = layout.getname(layout.get(screen))
    if name == "max" then
      tag_text:set_text(#tag:clients())
    else
    end
    layout_image = beautiful["layout_" .. name]
  end

  client.connect_signal("manage", refresh_layouts)
  screen:connect_signal("tag::history::update", refresh_layouts)
  for _, tag in ipairs(screen.tags) do
    tag:connect_signal("tagged", refresh_layouts)
    tag:connect_signal("untagged", refresh_layouts)
    tag:connect_signal("property::layout", refresh_layouts)
  end

  widget:add(tag_button)

  return widget
end

return layoutbox
