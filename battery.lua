local wibox     = require("wibox")
local gears     = require("gears")
local awful     = require("awful")
local beautiful = require("beautiful")

local battery = { }

function battery.new(args)

	if args.bg then
		bg = args.color
	else
		bg = beautiful.widget_bg
	end

	if args.fg then
		fg = args.color
	else
		fg = beautiful.widget_fg
	end

	if args.margins then
		margins = args.margins
	else
		margins = 10
	end

	local bat_command = [[bash -c '
while true ; do
  bat=$(cat /sys/class/power_supply/BAT0/capacity)
  echo $bat
  sleep 20
done
']]

	local widget     = wibox.layout.fixed.horizontal()
	local text       = wibox.widget.textbox()
	local margins    = wibox.container.margin(text, margins, margins)
	local background = wibox.container.background(margins)

	widget:add(background)
	background:set_bg(bg)

	pid = awful.spawn.with_line_callback(bat_command, {
		stdout = function(line)
			text:set_text('bat: '..line..'%')
		end
	})

	return widget
end

function battery.get_pid()
	return pid
end

return battery
